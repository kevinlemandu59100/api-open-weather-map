<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Affichage</title>
    <link rel="stylesheet" href="meteo.css" />
</head>

<body>

    <?php
    $ville = ""; // Valeur par défaut
    function cleanString($string)
    {
        // on supprime : majuscules ; / ? : @ & = + $ , . ! ~ * ( ) les espaces multiples et les underscore
        $string = strtolower($string);
        $string = preg_replace("/[^a-z0-9_'\s-]/", "", $string);
        $string = preg_replace("/[\s-]+/", " ", $string);
        $string = preg_replace("/[\s_]/", " ", $string);
        return $string;
    }
    if (isset($_POST['ville']) && isset($_POST['pays'])) {
        $ville = $_POST['ville'];
        $pays = $_POST['pays'];
        $cleanedVille = cleanString($ville);
        $cleanedPays = cleanString($pays);

        // Utilisez $cleanedVille pour former l'URL de l'API OpenWeatherMap et obtenir les données météorologiques
        $url = 'http://api.openweathermap.org/data/2.5/forecast/daily?q=' . urlencode($cleanedVille) . '&units=metric&cnt=7&lang=en&appid=c0c4a4b4047b97ebc5948ac9c48c0559';

        // Le reste du code pour obtenir et afficher les informations météorologiques
        $ville = $cleanedVille;
    }

    $city    = $cleanedVille;
    $country = $cleanedPays;
    $url     = 'http://api.openweathermap.org/data/2.5/forecast/daily?q=' . $city . ',' . $country . '&units=metric&cnt=7&lang=en&appid=c0c4a4b4047b97ebc5948ac9c48c0559';
    $json    = file_get_contents($url);
    $data    = json_decode($json, true);
    $data['city']['name'];
    // var_dump($data);

    echo ' <h1>Météo de la semaine pour la ville de ' . ucfirst($ville) . '.</h1>'; ?>
    <div class="weather-container">
        <?php
        foreach ($data['list'] as $value) {
            $timestamp = $value['dt'];
            $joursSemaine = array(
                'Monday' => 'Lundi',
                'Tuesday' => 'Mardi',
                'Wednesday' => 'Mercredi',
                'Thursday' => 'Jeudi',
                'Friday' => 'Vendredi',
                'Saturday' => 'Samedi',
                'Sunday' => 'Dimanche',

            );
            $descriptions = array(
                'Clear' => 'Dégagé',
                'Clouds' => 'Nuageux',
                'Rain' => 'Pluie',
                'Drizzle' => 'Bruine',
                'Thunderstorm' => 'Orage',
                'Snow' => 'Neige',
                'Mist' => 'Brume',

            );



            $jour = date('l', $timestamp);
            $jourEnFrancais = $joursSemaine[$jour];
            $date = $jourEnFrancais . ' ' . date('d-m-Y', $timestamp);

            echo '<img src="http://openweathermap.org/img/w/' . $value['weather'][0]['icon'] . '.png"
                    class="weather-icon" />';
            echo '    Méteo pour le ' . $date  . '<br />';
            echo 'Temps : ' . $descriptions[$value['weather'][0]['main']] . '<br />';
            echo 'Température minimale' . ' : ' . $value['temp']['min'] . '°C<br />';
            echo 'Température moyenne' . ' : ' . $value['temp']['day'] . '°C<br />';
            echo 'Température maximale' . ' : ' . $value['temp']['max'] . '°C<br />';
            echo 'Pression atmosphérique ' . ' : ' . $value['pressure'] . ' hPa<br />';
            echo 'Taux d\'humidité ' . ' : ' . $value['humidity'] . '%<br />';
            echo 'Vitesse du vent' . ' : ' . $value['speed'] . ' m/s<br />';
            echo 'Probabilité de pluie' . ' : ' . (isset($value['rain']) ? $value['rain'] : 0) . '%<br />';
            echo 'Niveau d\'UV ' . ' : ' . (isset($value['uvi']) ? $value['uvi'] : 0) . '<br />';
        }
        ?>
        <div>
</body>

</html>